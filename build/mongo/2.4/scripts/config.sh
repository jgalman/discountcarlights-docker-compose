#need to define $HOST_USER_NAME in docker run command
export CORE_DOMAIN="discountcarlights.staging.usautoparts.com"
export CART_DOMAIN="cart.discountcarlights.staging.usautoparts.com"
export ACCOUNT_DOMAIN="myaccount-staging-discountcarlights.usautoparts.com"
export CHECKOUT_DOMAIN="checkout.discountcarlights.staging.usautoparts.com"
export STATIC_DOMAIN="static.discountcarlights.staging.usautoparts.com"
